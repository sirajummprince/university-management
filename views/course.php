<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Department Add</title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">University Management System</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="../../index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo00"><i class="fa fa-fw fa-arrows-v"></i> Needed Dropdowns <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo00" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Day's</a>
                        </li>
                        <li>
                            <a href="Views/Department/department.php">Designation</a>
                        </li>
                        <li>
                            <a href="Views/Department/department.php">Rooms</a>
                        </li>
                        <li>
                            <a href="Views/Department/department.php">Semesters</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Grades</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Departments <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo" class="collapse">
                        <li>
                            <a href="../Department/department.php">Add Departments</a>
                        </li>
                        <li>
                            <a href="../Department/showDept.php">Show Departments</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-arrows-v"></i> Courses <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo1" class="collapse">
                        <li>
                            <a href="course.php">Add Courses</a>
                        </li>
                        <li>
                            <a href="showCourse.php">Show Courses</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-arrows-v"></i> Teachers <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo2" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Add Teachers</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Show Teachers</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-arrows-v"></i> Assign to Teachers <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo3" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Add Assign Teachers</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Show Assign Teachers</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-fw fa-table"></i> Course Statics </a>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-arrows-v"></i> Register Student <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo5" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Add Register Student</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Show Register Student</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo6"><i class="fa fa-fw fa-arrows-v"></i> Allocate Classroom <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo6" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Add Allocate Classroom</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Show Allocate Classroom</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo7"><i class="fa fa-fw fa-arrows-v"></i> Class and Roon Info <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo7" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Add Department</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Show Department</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo8"><i class="fa fa-fw fa-arrows-v"></i> Enroll In a Course <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo8" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Add Enroll Course</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Show Enroll Course</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#demo9"><i class="fa fa-fw fa-arrows-v"></i> Student Result <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="demo9" class="collapse">
                        <li>
                            <a href="Views/Department/department.php">Add Student Result</a>
                        </li>
                        <li>
                            <a href="Views/Department/showDept.php">Show Student Result</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Department
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-edit"></i>Add Course
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->






            <?php
            include_once ("../../vendor/autoload.php");
            use App\Course\Course;

            $data = new  Course();
            $allDept = $data->showDepartment();

            ?>


            <div class="container mainTable">
                <div class="row table">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label for="code" class="col-sm-12 title">Save Course</label>
                        </div>
                        <form method="post" action="courseStore.php">
                            <div class="form-group row">
                                <label for="code" class="col-sm-3 col-form-label txt">Code</label>
                                <div class="col-sm-9">
                                    <input type="text" name="code" class="form-control" id="" placeholder="Code">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label txt">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control" id="" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="credit" class="col-sm-3 col-form-label txt">Credit</label>
                                <div class="col-sm-9">
                                    <input type="text" name="credit" class="form-control" id="" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="desc" class="col-sm-3 col-form-label txt">Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="desc" id="exampleTextarea" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="credit" class="col-sm-3 col-form-label txt">Department</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="exampleSelect1" name="dept">
                                        <?php
                                        foreach ($allDept as $data) {
                                            ?>
                                            <option> <?php echo $data['name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="credit" class="col-sm-3 col-form-label txt">Semester</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="exampleSelect1"  name="seme">
                                        <?php
                                        foreach ($allDept as $data) {
                                            ?>
                                            <option> <?php echo $data['code']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" value="Save" class="btn btn-default saveBtn"  id="">
                                </div>
                            </div>
                        </form>
                        <div class="msg">
                            <?php
                            @session_start();
                            if(isset($_SESSION['Msg'])){
                                echo $_SESSION['Msg'];
                                unset($_SESSION['Msg']);
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div> <br><br><br><br>


            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../js/bootstrap.min.js"></script>

</body>

</html>




<!-- Style Css For Table-->
<style>
    .mainTable{
        width: 45%;
        background-color: #fff;
        box-shadow: 1px 3px 8px #999;
        border: 1px solid #ddd;
    }
    .table{
        margin: auto;
        width: 70%;
        border-radius:4px;
    }
    .title{
        font-size: 22px;
        text-align: center;
        height: 60px;
        line-height: 60px;
        border-bottom: 1px solid #999;
    }
    .txt{
        font-size: 16px;
    }
    .saveBtn{
        float: right;
    }
    .msg{
        height: 50px;
        line-height: 50px;
        text-align: center;

    }
</style>